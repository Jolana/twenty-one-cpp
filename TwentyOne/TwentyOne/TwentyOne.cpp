#include <iostream>
#include <algorithm>  
#include <ctime>

using namespace std;

// Getting a new card
int getNewCard(bool *active, int length) {
    srand(time(NULL));
    int randomCards;
    do
    {
        randomCards = rand() % 36;
    } while (active[randomCards] == false);
    active[randomCards] = false;
    return randomCards;
}

int main()
{
    cout << "\n*** Twenty One! ***\n\n";

    // Creating a deck of cards
    string suits[4] = { "diamonds", "clubs", "hearts", "pades" };
    string value[9] = { "6", "7", "8", "9", "10", "J", "Q", "K", "T" };
    int score[9] = { 6,7,8,9,10,2,3,4,11 };

    string deck[36];
    int deckValue[36];
    bool deckActive[36];
    int count = 0;

    for (int i = 0; i < 4; ++i)
    {
        for (int j = 0; j < 9; ++j)
        {
            deck[count] = value[j] + " " + suits[i];
            deckValue[count] = score[j];
            ++count;
        }
    }


    int playerScore = 0;    // Player points
    int ˝roupierScore = 0;  // Croupier points
    bool lose = false;      // Did the player lose?

    char stop = 'y';        // Take another card yes -> y  no-> n
    int nextCard;           // Next card

    // The player's first card
    nextCard = getNewCard(deckActive, 36);
    cout << deck[nextCard] << endl;
    playerScore += deckValue[nextCard];
    cout << "*** Take another card yes -> y  no-> n ***" << endl;
    cin >> stop;

    // The first card is croupier
    int nextCardCroupier = getNewCard(deckActive, 36);
    /*cout << nextCard << " / " << deck[nextCard] << " / " << deckValue[nextCard] << endl;*/
    ˝roupierScore += deckValue[nextCardCroupier];

    // The player takes more cards
    while (stop == 'y')
    {
        nextCard = getNewCard(deckActive, 36);
        cout << deck[nextCard] << endl;
        playerScore += deckValue[nextCard];
        if (playerScore == 21) {
            cout << "\n*** You win! ***";
            lose = true;
            break;
        }
        if (playerScore > 21) {
            cout << "\n*** You lose! ***";
            lose = true;
            break;
        }
        cout << "\n*** Take another card yes -> y  no-> n ***" << endl;
        cin >> stop;
    }
    
    // If the player does not score> = 21 points then the croupier starts playing 
    if (lose == false) {
        cout << deck[nextCardCroupier] << endl;

        while (˝roupierScore <= 16)
        {
            nextCard = getNewCard(deckActive, 36);
            cout << deck[nextCard] << endl;
            ˝roupierScore += deckValue[nextCard];
        }

        if ((˝roupierScore > 21) || (playerScore > ˝roupierScore)) {
            cout << "\n*** You win! ***";
        }
        else
        {
            cout << "\n*** You lose! ***";
        }
    }
    
    cout << "\n\n*** End Game! ***";
}
